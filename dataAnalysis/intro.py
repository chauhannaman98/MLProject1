#!/usr/bin/env python
# coding: utf-8

# In[4]:


import numpy as np


# In[3]:


import pandas as pd


# In[4]:


import scipy as sp
import matplotlib as mpl
import seaborn as sns


# In[5]:


#read csv file
df = pd.read_csv("http://rcs.bu.edu/examples/python/data_analysis/Salaries.csv")


# In[6]:


#list first 5 records
df.head(20)      # syntax : df.head(number of entries)


# In[7]:


# Check a particular column's data type
df['salary'].dtype


# In[8]:


df.dtypes


# In[9]:


# Check number of dimensions
df.ndim


# In[10]:


# return a tuple representing the dimensionality
df.shape


# In[11]:


# numpy representation of data
df.values


# In[12]:


# return max/min values for all columns
df.max


# In[13]:


# return descriptive stats
df.describe()


# In[14]:


# return standard deviation
df.std()


# In[15]:


# return mean
df.mean()


# In[16]:


# return median
df.median()


# In[17]:


# return a random sample of data frame
df.sample(3)


# In[18]:


# drop all the records with missing values
df.dropna()


# In[19]:


# selecting a column using as column name
df['sex']


# In[20]:


# using column name as attribute
df.sex


# In[21]:


# return number of values in columns
df.count()


# In[22]:


# Groupby method
# split the data into groups based on some criteria
# calculate statistics or apply function to each group
# group data using rank
df_rank = df.groupby(['rank'])


# In[23]:


# Calculate mean value for each numneric column per each group
df_rank.mean()


# In[24]:


# Calculate mean salary for each professor rank
df.groupby('rank')[['salary']].mean()


# In[25]:


# groupby performance notes:
#   - no grouping/splitting occurs unit it's needed. Creating the groupby object only verifies that you have passed a valid mapping
#   - by default, the group keys are sorted during the groupby operation. You may want to pass sort=false for potential speedup
# Calculate mean salary for each professor rank:
df.groupby(['rank'],sort=False)[['salary']].mean()


# In[26]:


# FILTERING
# Calculate salary greater than 120000
df_sub = df[df['salary']>120000]
print(df_sub)


# In[27]:


# Select only those rows that contain female professors:
df_f = df[df['sex']=='Female']
print(df_f)


# In[28]:


# SLICING
# When selecting one column, it is possible to use single set of brackets, but the resulting object will be a Series (not a DataFrame) :
# Select column salary
df[['salary']]


# In[29]:


# When we need to select more than one column and/or make the output to be a DataFrame, we should use double brackets:
# Select column salary:
df[['rank','salary']]


# In[30]:


# SELECTING ROWS
# Select rows by their position
df[10:20]


# In[31]:


# if we need to select a range of rows, using their labels we can use method loc:
# Select rows by their labels:
df.loc[10:20,['rank','sex','salary']]


# In[32]:


# method iloc
# Select rows by their labels:
df.iloc[10:20,[0,3,4,5]]


# In[33]:


# SORTING
# Create a new data frame from the original sorted by the column Salary
df_sorted = df.sort_values(by='salary')
df_sorted.head()


# In[37]:


flights = pd.read_csv("http://rcs.bu.edu/examples/python/data_analysis/flights.csv")


# In[38]:


flights[flights.isnull().any(axis=1)].head()


# In[47]:


import seaborn as sns


# In[48]:


sns.distplot(df['salary']);


# In[49]:


sns.set_style("whitegrid")

ax = sns.barplot(x='rank',y ='salary', data=df, estimator=len)


# In[50]:


sns.violinplot(x = "salary", data=df)


# In[51]:


sns.regplot(x='service', y='salary', data=df)


# In[52]:


sns.pairplot(df)


# In[ ]:




