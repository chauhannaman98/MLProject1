#!/usr/bin/env python
# coding: utf-8

# In[1]:


import matplotlib.pyplot as plt
import numpy as np


# In[3]:


x = np.arange(1,10)  # values from 1 to 10


# In[4]:


y1=x+2
y2=x*6
y3=x/5


# In[5]:


plt.plot(x,y1,x,y2,x,y3)
plt.show()


# In[6]:


plt.bar([1,2,3],[5,2,1])
plt.xlabel("This is x-axis label")
plt.ylabel("This is y-axis label")
plt.title("Simple bar chart")
plt.show()


# In[7]:


percentage = [60,30,10]
language = ["Python","Java","C++"]
plt.pie(percentage,labels=language)
plt.title("Simple pie chart")
plt.show()


# In[8]:


x = np.random.rand(100)
y = np.random.rand(100)
plt.title("Simple scatter plot")
plt.scatter(x,y)
plt.show()


# In[9]:


import seaborn as sns
Tips = sns.load_dataset('tips')
Tips.head()


# In[17]:


sns.distplot(Tips['total_bill']);


# In[21]:


sns.jointplot(x='total_bill',y='tip',data=Tips)


# In[22]:


sns.pairplot(data=Tips);


# In[23]:


sns.boxplot(data=Tips);


# In[24]:


#  categorial plot
sns.barplot(x='sex',y='total_bill',data=Tips);


# In[ ]:




